{-# Language TemplateHaskell #-}
{-# Language InstanceSigs #-}
module Browser where
import SuffixLenses
import Data.Char
import Control.Applicative
import Control.Monad.Reader
import Data.Monoid ((<>))
import Lens.Micro ((^.), (&), (.~), (%~))

data Tree a = EmptyTree | Root a [Tree a] deriving (Show,Eq)

instance Functor Tree where
 fmap f EmptyTree = EmptyTree
 fmap f (Root x rest) = Root (f x) ((fmap f) <$> rest)

instance Monoid z => Monoid (Tree z) where
 mempty = EmptyTree
 mappend EmptyTree t = t
 mappend t EmptyTree = t
 mappend (Root a ts) (Root b tss) = Root (a<>b) (liftA2 (<>) tss ts)


data MyList a = Mt | Mycons a (MyList a) deriving (Show,Eq)

instance Functor MyList where
 fmap _ Mt = Mt
 fmap f (Mycons a xs) = Mycons (f a) (f <$> xs)


ml::MyList Int
ml = h2m [1..10]

h2m::[a]->MyList a
h2m [] = Mt
h2m (x:xs) = Mycons x (h2m xs)

instance Monoid (MyList a) where
 mempty = Mt
 mappend Mt u = u
 mappend u Mt = u
 mappend (Mycons a xs) u = Mycons a (mappend xs u)

instance Applicative MyList where
 pure a = Mycons a Mt
 (<*>):: MyList (a->b)-> MyList a -> MyList b
 _  <*> Mt = Mt
 Mt <*> _ = Mt
 (Mycons a xs) <*> list2 = (a <$> list2) <> (xs <*> list2)

instance Foldable MyList where
 foldMap :: Monoid m => (a -> m) -> MyList a -> m
 foldMap f Mt = mempty
 foldMap f (Mycons a xs) = (f a) <> (foldMap f xs)

instance Traversable MyList where
 traverse :: Applicative f => (a -> f b) -> MyList a -> f (MyList b)
 traverse f Mt = pure Mt
 traverse f (Mycons a xs) = pure <$> (f a) <* (traverse f xs)

data MyMaybe a = NOTH | JUST  a deriving (Show,Eq)

instance Functor MyMaybe where
 fmap f NOTH = NOTH
 fmap f (JUST a) = JUST (f a)

instance Applicative MyMaybe where
 pure a = JUST a
 NOTH <*> _ = NOTH
 _ <*> NOTH = NOTH
 (JUST f) <*> (JUST a) = JUST (f a)


{-
instance Monoid b=>Applicative Tree where
 pure a = Root a []
 (<*>)::(Monoid b)=> Tree (a->b)-> Tree a -> Tree b
 _  <*> EmptyTree = EmptyTree
 EmptyTree <*> _ = EmptyTree
 (Root a ts) <*> t2 =foldl (\resultTree funcTree-> (funcTree <*> t2) <> resultTree) (a <$> t2) ts
-}







t::Tree String
t = Root "3" [Root "3" [], Root "34" [Root "10" [],EmptyTree]]






data BrowserProg = CommandSeq {
                   initCommand::LoadCommand
                 , restCmds::[Command]
                 }deriving (Show,Eq)

data LoadCommand = LoadCommand String Int deriving (Show,Eq)

data Command = RIGHT | LEFT | DISPLAY  deriving (Show,Eq)

data Browser = Browser{
               buffer::String
             , outPut::Char
             , cursor::Int
             , prog::BrowserProg
             , instructionPointer::Int
             , doneFlag::Bool
             } deriving (Show,Eq)

suffixLenses ''Browser
suffixLenses ''BrowserProg

p1::BrowserProg
p1 = CommandSeq (LoadCommand "abcdefghijk" 4 ) [DISPLAY,LEFT,DISPLAY,LEFT,LEFT,RIGHT,DISPLAY]

createBrowser::BrowserProg->Browser
createBrowser p@(CommandSeq (LoadCommand s pos) cmds) = 
 Browser s ' ' pos p 0 False

b1::Browser
b1 = createBrowser p1

browse::Int->Browser-> Browser
browse conf brws = case (brws ^. doneFlagL) of 
 True -> brws
 _-> case ((length $ brws ^.progL ^. restCmdsL) <= brws ^.instructionPointerL) of
  True -> brws & doneFlagL .~ True
  _-> case ((brws ^.progL ^. restCmdsL) !! (brws ^. instructionPointerL)) of 
    DISPLAY -> brws & outPutL .~ (readConf conf $ ((brws ^. bufferL) !! (brws ^. cursorL))) & incrementPointer 
    LEFT  |(brws ^. cursorL > 0) -> brws & cursorL %~ (pred) &incrementPointer 
    RIGHT |(length $ brws ^. bufferL) > (1 + brws ^. cursorL) ->brws & cursorL %~ (+1) & incrementPointer
    _-> brws
  where
   incrementPointer = instructionPointerL %~ (+1)


executeCmds::Int->(Char->String)->Browser->[String]
executeCmds conf decorator brws= (decorator . outPut) <$> browsers
 where
  browsers =takeWhile (\x-> not $ x ^. doneFlagL) . iterate (browse conf) $ brws

f1::Int->(Char->String)->Browser->[String]
f1 i dec brws = ss:(f2 i dec brws')
 where
  ss = dec.outPut $ brws
  brws' = browse i brws

f2::Int->(Char->String)->Browser->[String]
f2 i dec brws = case (brws ^. doneFlagL) of 
 True -> []
 _ -> f1 i dec brws



decorate::Char->String
decorate c = s
 where
  s = header <> pure c <> footer
  header = "THE BEST BROWSER IN THE WORLD(version3.2.1)\n\n"
  footer = "\n\nYOU ARE BROWSING - " <> pure c <> "\n\n"

decorate1::String->String
decorate1 s = decoratedStr
 where
  decoratedStr = toLower <$> s

readConf::Int->Char->Char
readConf 0 c = toLower c
readConf 1 c = toUpper c
readConf _ c = c



u =undefined

--------------------------------------------------------
{-
instance MonadReader Int [] where
 ask =[Int]
-}








